/*
Copyright 2012 Jun Wako <wakojun@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "config_common.h"

#define VENDOR_ID       0xBEEF
#define PRODUCT_ID      0x2062
#define DEVICE_VER      0x0001
#define MANUFACTURER    Unkeyboard
#define PRODUCT         Atreus62N
#define DESCRIPTION     Unkeyboard Atreus N 62 Rev. 0

#define MATRIX_ROWS 5
#define MATRIX_COLS 14

#define MATRIX_COL_PINS { B10, A1, A2, B0, A7, A6, C13, B9, B3, A8, B15, B14, B13, B12 }
#define MATRIX_ROW_PINS { A3, A15 , B5, B7, A4 }

#define DIODE_DIRECTION COL2ROW

#define DEBOUNCE 5
#define LOCKING_SUPPORT_ENABLE
#define LOCKING_RESYNC_ENABLE
